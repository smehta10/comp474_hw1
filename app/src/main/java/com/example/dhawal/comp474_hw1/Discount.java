package com.example.dhawal.comp474_hw1;

/**
 * Created by ${Shipra} on ${03/13/2017}.
 */

public interface Discount {

	public void applyDiscount(Item item);	
	
}
