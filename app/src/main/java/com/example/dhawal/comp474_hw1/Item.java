package com.example.dhawal.comp474_hw1;
/**
 * Created by ${Shipra} on ${03/13/2017}.
 */

public class Item {
	
	private int id = 0;
	private String itemName = "";
	private int quantity = 0;
	private double totalPrice;
	
	public double getTotalPrice() {
		return totalPrice;
	}


	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Item(){}

	public Item(int id, String itemName, int quantity, double totalPrice) {
		this.id=id;
		this.itemName = itemName;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}
