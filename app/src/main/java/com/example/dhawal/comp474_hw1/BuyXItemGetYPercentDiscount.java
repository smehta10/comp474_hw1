package com.example.dhawal.comp474_hw1;
public class BuyXItemGetYPercentDiscount implements Discount {

	private int XminItem;
	private int XmaxItem;
	private double YDiscount;

	public BuyXItemGetYPercentDiscount(int xminItem,int xmaxItem, double yDiscount) {
		XminItem = xminItem;
		XmaxItem = xmaxItem;
		YDiscount = yDiscount;
	}

	@Override
	public void applyDiscount(Item item) {

		int totalQuantity = item.getQuantity();
		double unitPrice = item.getTotalPrice() / item.getQuantity();
		while (totalQuantity > 1) {
			double price = item.getTotalPrice();
			item.setTotalPrice(price - (unitPrice / (100/YDiscount)));

		}

	}



}
