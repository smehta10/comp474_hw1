package com.example.dhawal.comp474_hw1;

/**
 * Created by ${Shipra} on ${03/13/2017}.
 */

public class MembersClubDiscount implements Discount {
	
	private double discountPercentage;
	 
	public MembersClubDiscount(double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	@Override
	public void applyDiscount(Item item) {
		int totalQuantity = item.getQuantity();
		double unitPrice = item.getTotalPrice() / item.getQuantity();
		while (totalQuantity > 1) {
			double price = item.getTotalPrice();
			item.setTotalPrice(price - (unitPrice / (100 / discountPercentage)));
		}

	}

}
