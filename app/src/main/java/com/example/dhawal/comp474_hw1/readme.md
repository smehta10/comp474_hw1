#commands to compile source files
javac comp474/hw1/Item.java
javac comp474/hw1/Customer.java
javac -cp ./lib/junit-4.12.jar:./lib/hamcrest-all-1.3.jar:./ comp474/hw1/TestItem.java
javac -cp ./lib/junit-4.12.jar:./lib/hamcrest-all-1.3.jar:./ comp474/hw1/TestCustomer.java
javac -cp ./lib/junit-4.12.jar:./lib/hamcrest-all-1.3.jar:./ comp474/hw1/TestShoppingCart.java
javac -cp ./lib/junit-4.12.jar:./lib/hamcrest-all-1.3.jar:./ comp474/hw1/TestRunner.java

 
#Command to run test cases 
java -cp ./lib/junit-4.12.jar:./lib/hamcrest-all-1.3.jar:./ comp474/hw1/TestRunner