package com.example.dhawal.comp474_hw1;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Shipra on 3/13/17.
 */
public class ShoppingCart {

	private List<Item> itemList = new ArrayList<Item>();
	private double totalCartValue;
	private Discount discount;
	private SalesTax salesTax;

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}

	public SalesTax getSalesTax() {
		return salesTax;
	}

	public void setSalesTax(SalesTax salesTax) {
		this.salesTax = salesTax;
	}

	public void setTotalCartValue(double totalCartValue) {
		this.totalCartValue = totalCartValue;
	}

	public Discount getDiscount() {
		return discount;
	}

	public void setDiscount(Discount discount) {
		this.discount = discount;
	}

	public int getItemCount() {
		return itemList.size();
	}

	public void addItem(Item item) {
		if(discount != null){
			discount.applyDiscount(item);//apply discount
		}

		if(salesTax!=null){
			salesTax.applySalesTax(item);//apply sales tax
		}
		itemList.add(item);
	}

	public double getTotalCartValue() {

		if (itemList.size() > 0) {
			for (Item item : itemList) {
				totalCartValue = totalCartValue + item.getTotalPrice();
			}
		}
		return totalCartValue;
	}
	public ShoppingCart(Customer customer) { }

	public ShoppingCart() { }

	public float calcPurchasePrice() { return 0.0f;}

	public float calcPuchasePrice(List<String> ProductIDs, Customer customer) {return 0.0f;}


}
