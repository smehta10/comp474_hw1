package com.example.dhawal.comp474_hw1;

/**
 * Created by ${Shipra} on ${03/13/2017}.
 */

public class LocalSalesTax implements SalesTax {
	
	private double salesTaxPercentage;
	 
	public LocalSalesTax(double salesTaxPercentage) {
		this.salesTaxPercentage = salesTaxPercentage;
	}

	@Override
	public void applySalesTax(Item item) {

		int totalQuantity = item.getQuantity();
		double unitPrice = item.getTotalPrice() / item.getQuantity();
		while (totalQuantity > 1) {
			double price = item.getTotalPrice();
			item.setTotalPrice(price + (unitPrice / (100/salesTaxPercentage)));

		}


	}

}
