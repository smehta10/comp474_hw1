package com.example.dhawal.comp474_hw1;

/**
 * Created by ${Shipra} on ${03/13/2017}.
 */
public class Customer {
	private int id = 0;
    private String firstName = "";
    private String lastName = "";
    private boolean isClubMember;
    private boolean isTaxExempt;
    private ShoppingCart cart;
     
    public Customer() {}
    
    public Customer(int id,String firstName, String lastName, boolean isClubMember, boolean isTaxExempt, ShoppingCart cart) {
		super();
		this.id=id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.isClubMember = isClubMember;
		this.isTaxExempt = isTaxExempt;
		this.cart = cart;
	}
	public ShoppingCart getCart() {
		return cart;
	}
	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public boolean isClubMember() {
		return isClubMember;
	}
	public void setClubMember(boolean isClubMember) {
		this.isClubMember = isClubMember;
	}
	public boolean isTaxExempt() {
		return isTaxExempt;
	}
	public void setTaxExempt(boolean isTaxExempt) {
		this.isTaxExempt = isTaxExempt;
	}
}
