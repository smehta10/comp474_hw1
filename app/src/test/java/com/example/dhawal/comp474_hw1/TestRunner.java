package com.example.dhawal.comp474_hw1;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

/**
 * Created by ${Shipra} on ${03/13/2017}.
 */
public class TestRunner {

    public static void main(String[] args) {

        //Create the test runner for TestItem and print out the results
        System.out.println("\n ========================= begin tests for Item class =========================");
        Result testItemResult = JUnitCore.runClasses(TestItem.class);
        for (Failure failure : testItemResult.getFailures()) {
            System.out.println("failed: " + failure.toString());
        }
        if(testItemResult.wasSuccessful()){
            System.out.println("All tests for Item class finished successfully...");
        } else {
            System.out.println( testItemResult.getFailureCount() + " test case(s) for Item class are failed...");
        }

        //Create the test runner for TestCustomer and print out the results
        System.out.println("\n ========================= begin tests for Customer class =========================");
        Result testCustomerResult = JUnitCore.runClasses(TestCustomer.class);
        for (Failure failure : testCustomerResult.getFailures()) {
            System.out.println("failed: " + failure.toString());
        }
        if(testCustomerResult.wasSuccessful()){
            System.out.println("All tests for Customer class finished successfully...");
        } else {
            System.out.println( testCustomerResult.getFailureCount() + " test case(s) for Customer class are failed...");
        }

        //Create the test runner for TestShoppingCart and print out the results
        System.out.println("\n ========================= begin tests for ShoppingCart class =========================");
        Result testShoppingCartResult = JUnitCore.runClasses(TestShoppingCart.class);
        for (Failure failure : testShoppingCartResult.getFailures()) {
            System.out.println("failed: " + failure.toString());
        }
        if(testShoppingCartResult.wasSuccessful()){
            System.out.println("All tests for ShoppingCart class finished successfully...");
        } else {
            System.out.println( testShoppingCartResult.getFailureCount() + " test case(s) for ShoppingCart class are failed...");
        }

    }
}
