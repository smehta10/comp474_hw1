package com.example.dhawal.comp474_hw1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ${Shipra} on ${03/13/2017}.
 */
public class TestCustomer {

    @Test
    public void testConstructor() {


        //Create new object with non-default values
    	ShoppingCart cart = new ShoppingCart();
    	Customer customer = new Customer(1, "Byambatsog", "Chimed", true, true,cart);
        
        //Test that the object has the correct values.
        assertEquals(1, customer.getId());
        assertEquals("Byambatsog", customer.getFirstName());
        assertEquals("Chimed", customer.getLastName());
        assertEquals(true, customer.isClubMember());
        assertEquals(true, customer.isTaxExempt());
    }

    @Test
    public void testEmptyConstructor() {

        //Create new object with default values
        Customer customer = new Customer();

        //Test that the object has the default values.

        assertEquals(0, customer.getId());
        assertEquals("", customer.getFirstName());
        assertEquals("", customer.getLastName());
        assertEquals(false, customer.isClubMember());
        assertEquals(false, customer.isTaxExempt());
    }

    @Test
    public void testSetId() {

        //Create new object with default values
        Customer customer = new Customer();

        //Set with a non-default value
        customer.setId(4);

        //Test that the object has the correct value.
        assertEquals(4, customer.getId());
    }

    @Test
    public void testSetFirstName() {

        //Create new object with default values
        Customer customer = new Customer();

        //Set with a non-default value
        customer.setFirstName("Shipra");

        //Test that the object has the correct value.
        assertEquals("Shipra", customer.getFirstName());
    }

    @Test
    public void testSetLastName() {

        //Create new object with default values
        Customer customer = new Customer();

        //Set with a non-default value
        customer.setLastName("Mehta");

        //Test that the object has the correct value.
        assertEquals("Mehta", customer.getLastName());
    }

    @Test
    public void testSetIsClubMember() {

        //Create new object with default values
        Customer customer = new Customer();

        //Set with a non-default value
        customer.setClubMember(true);

        //Test that the object has the correct value.
        assertEquals(true, customer.isClubMember());
    }

    @Test
    public void testSetIsTaxExempt() {

        //Create new object with default values
        Customer customer = new Customer();

        //Set with a non-default value
        customer.setTaxExempt(true);

        //Test that the object has the correct value.
        assertEquals(true, customer.isTaxExempt());
    }
}
