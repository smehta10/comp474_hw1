package com.example.dhawal.comp474_hw1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Created by ${Shipra} on ${03/13/2017}.
 */
public class TestItem {

    @Test
    public void testConstructor() {

        //Create new object with non-default values
        Item item = new Item(001, "Iphone 7",1, 799.99f);

        //Test that the object has the correct values.
        assertEquals(001, item.getId());
        assertEquals("Iphone 7", item.getItemName());
        assertEquals(1,item.getQuantity());
        assertEquals(799.99f, item.getTotalPrice(), 0.0f);
    }

    @Test
    public void testEmptyConstructor() {

        //Create new object with default values
        Item item = new Item();

        //Test that the object has the default values.
        assertEquals(0, item.getId());
        assertEquals("", item.getItemName());
        assertEquals(0,item.getQuantity());
        assertEquals(0.0f, item.getTotalPrice(), 0.0f);
    }

    @Test
    public void testSetId() {

        //Create new object with default values
        Item item = new Item();

        //Set with a non-default value
        item.setId(002);

        //Test that the object has the correct value.
        assertEquals(002, item.getId());
    }

    @Test
    public void testSetName() {

        //Create new object with default value
        Item item = new Item();

        //Set with a non-default value
        item.setItemName("Iphone 7");

        //Test that the object has the correct value.
        assertEquals("Iphone 7", item.getItemName());
    }

    @Test
    public void testSetPrice() {

        //Create new object with default value
        Item item = new Item();

        //Set with a non-default value
        item.setTotalPrice(699.99f);

        //Test that the object has the correct value.
        assertEquals(699.99f, item.getTotalPrice(), 0.0f);
    }

    @Test
    public void testSetNegativePrice() {

        //Create new object with default value
        Item item = new Item();

        //Set with an invalid value
        item.setTotalPrice(-1.0f);

        //Test that the object and return value have the correct value.
        assertEquals("Setting negative value (-1) failed", (new Item()).getTotalPrice(), item.getTotalPrice(), 0.0f);
    }

}
