package com.example.dhawal.comp474_hw1;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

/**
 * Created by Shipra on 03/13/17.
 */
public class TestShoppingCart {

	// Created MockDatabase of 50 items using HashMap.
    private HashMap<Integer, Item> imockDatabase;

	//Implemented exception handling in order to limit the 50 items limit in single cart.
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() throws Exception {

		//added 50 items to HashMap in order to make a list of 50 items.
        imockDatabase = new HashMap<>();
        imockDatabase.put(1, new Item(1, "Cafe Bustelo Coffee",1, 7.39f));
        imockDatabase.put(2, new Item(2, "Coffee Creamer",1, 4.49f));
        imockDatabase.put(3, new Item(3, "Orange Juice",1, 2.19f));
        imockDatabase.put(4,new Item(4, "Sparkling water",6,3.79f));
        imockDatabase.put(5,new Item(5, "Bread",3, 3.19f));
        imockDatabase.put(6, new Item(6, "Breakfast Cereal",8, 3.99f));
        imockDatabase.put(7, new Item(7, "Chili Beans",1, 3.29f));
        imockDatabase.put(8, new Item(8, "Waffle", 1, 6.29f));
        imockDatabase.put(9, new Item(9, "Whole Milk",5,5.09f));
        imockDatabase.put(10, new Item(10, "Eggs",4, 4.99f));
        imockDatabase.put(11, new Item(11, "Sour Cream", 5, 2.89f));
        imockDatabase.put(12, new Item(12, "Greek Yogurt", 6,1.0f));
        imockDatabase.put(13, new Item(13, "Bananas", 1, 0.36f));
        imockDatabase.put(14, new Item(14, "Avocados",1, 2.79f));
        imockDatabase.put(15, new Item(15, "Apples",1,1.32f));
        imockDatabase.put(16, new Item(16, "Grapes",1,3.98f));
        imockDatabase.put(17, new Item(17, "Asparagus",1,2.99f));
        imockDatabase.put(18, new Item(18, "Celery",1,3.09f));
        imockDatabase.put(19, new Item(19, "Tomatoes",1,5.80f));
        imockDatabase.put(20, new Item(20, "Brussel Sprouts",1,3.71f));
        imockDatabase.put(21, new Item(21, "Potatoes",1,7.39f));
        imockDatabase.put(22, new Item(22, "Buiscuits",1, 4.49f));
        imockDatabase.put(23, new Item(23, "Creesps",1,2.19f));
        imockDatabase.put(24, new Item(24, "Salad",1, 3.79f));
        imockDatabase.put(25, new Item(25, "Aspharagus",1, 3.19f));
        imockDatabase.put(26, new Item(26, "Ketchup",1, 3.99f));
        imockDatabase.put(27, new Item(27, "Romano Tomatoes",1, 3.29f));
        imockDatabase.put(28, new Item(28, "Onions",1, 6.29f));
        imockDatabase.put(29, new Item(29, "Black Olives",1, 5.09f));
        imockDatabase.put(30, new Item(30, "Capsicum",1, 4.99f));
        imockDatabase.put(31, new Item(31, "Mayonese",1, 2.89f));
        imockDatabase.put(32, new Item(32, "Wheat Flour",1, 10.0f));
        imockDatabase.put(33, new Item(33, "Rice",1, 20.36f));
        imockDatabase.put(34, new Item(34, "Bread Crums", 1, 2.79f));
        imockDatabase.put(35, new Item(35, "Mangoes",1, 0.00f ));
        imockDatabase.put(36, new Item(36, "Sugar",1,5.98f));
        imockDatabase.put(37, new Item(37, "Salt",1, 2.99f));
        imockDatabase.put(38, new Item(38, "Choco powder",1,3.09f));
        imockDatabase.put(39, new Item(39, "Oil", 1, 5.80f));
        imockDatabase.put(40, new Item(40, "Nutella",1, 3.71f)); 
        imockDatabase.put(41, new Item(41, "Carrots",1, 4.99f));
        imockDatabase.put(42, new Item(42, "Wheeping Cream",1,1.0f));
        imockDatabase.put(43, new Item(43, "Noodles",1, 0.00f));
        imockDatabase.put(44, new Item(44, "Okra",1, 2.00f));
        imockDatabase.put(45, new Item(45, "Opo",1, 1.00f));
        imockDatabase.put(46, new Item(46, "Coriander leaves",1,3.00f));
        imockDatabase.put(47, new Item(47, "Palak",1,2.00f));
        imockDatabase.put(48, new Item(48, "Chillies",1,3.00f));
        imockDatabase.put(49, new Item(49, "Cottage Cheese",1, 5.00f));
        imockDatabase.put(50, new Item(50, "Butter",1, 3.00f)); 
        imockDatabase.put(51, new Item(51, "Coriander",1, 2.00f));

     /*    Created mockdatabase for customers if incase needed in the future.
     	private HashMap<Integer, Customer> cmockDatabase;
  
      * cmockDatabase.put(1, new Customer(1, "Christine", "Kreger", true, true,new ShoppingCart()));
        cmockDatabase.put(2, new Customer(2, "Joe", "Napolitano", true, true,new ShoppingCart()));*/
       /* cmockDatabase.put(3, new Customer(3, "Sonia", "Cheng", true, true,new ShoppingCart()));
        cmockDatabase.put(4, new Customer(4, "Rod", "Mustard", true, true,new ShoppingCart()));
        cmockDatabase.put(5, new Customer(5, "Sarah", "Petschke", true, false,new ShoppingCart()));
        cmockDatabase.put(6, new Customer(6, "Faith", "Orlando", true, false,new ShoppingCart()));
        cmockDatabase.put(7, new Customer(7, "Kristin", "Bolton", true, false,new ShoppingCart()));
        cmockDatabase.put(8, new Customer(8, "Jason", "Peesel", true, false,new ShoppingCart()));
        cmockDatabase.put(9, new Customer(9, "Clark", "Dandingar", false, true,new ShoppingCart()));
        cmockDatabase.put(10, new Customer(10, "Nick", "Boehing", false, true,new ShoppingCart()));
        cmockDatabase.put(11, new Customer(11, "Sui", "Lenckus", false, true,new ShoppingCart()));
        cmockDatabase.put(12, new Customer(12, "Don", "Bradon", false, true,new ShoppingCart()));
        cmockDatabase.put(13, new Customer(13, "Gary", "Cox", false, false,new ShoppingCart()));
        cmockDatabase.put(14, new Customer(14, "Amy", "Evans", false, false,new ShoppingCart()));
        cmockDatabase.put(15, new Customer(15, "Kelly", "Burkes", true, true,new ShoppingCart()));
        cmockDatabase.put(16, new Customer(16, "Rose", "Wis", true, false,new ShoppingCart()));
        cmockDatabase.put(17, new Customer(17, "Ben", "Machi", true, true,new ShoppingCart()));
        cmockDatabase.put(18, new Customer(18, "Ken", "Pitts", true, false,new ShoppingCart()));
        cmockDatabase.put(19, new Customer(19, "Ronda", "Roberts", true, true,new ShoppingCart()));
        cmockDatabase.put(20, new Customer(20, "Brain", "Pettry", false, false,new ShoppingCart()));*/

    }

    @After
    public void tearDown() throws Exception {
        imockDatabase = null;
    }
    
    
    @Test
    public void testCreateEmptyShoppingCart(){
    	
    	// Create new object of ShoppingCart default values
    	ShoppingCart shoppingcart = new ShoppingCart();
    	
    	//test that the shopping cart is empty
    	assertEquals(0,shoppingcart.getItemCount());
    }
    
        
    @Test
	public void testAddSingleItemToShoppingCart() {

		// Create new object of ShoppingCart default values
		ShoppingCart cart = new ShoppingCart();
		Item item = imockDatabase.get(1);

		//Add single item to the cart and test number of counts in the shopping cart.
		cart.addItem(item);
		assertEquals(1, cart.getItemCount());
		assertEquals(7.39, cart.getTotalCartValue(),0.01);
	}
    
    
    @Test
	public void testAddDifferentItemToTheCart(){

		// Create new object of ShoppingCart default values
		ShoppingCart cart = new ShoppingCart();

		//Create 2 objects of Item and get from the mockdatabase.
		Item creamer = imockDatabase.get(2);
		Item juice = imockDatabase.get(3);

		//Add 2 items to the cart.
		cart.addItem(creamer);
		cart.addItem(juice);

		// test the number of count in the shopping cart.
		assertEquals(2, cart.getItemCount());
		assertEquals(6.68, cart.getTotalCartValue(),0.01);
	}
    
    
	@Test
	public void testAddMultipleQtyAndApplyDiscountToCart() {

		//Created new object of Discount to implement minXitem, maxXitem and YDiscount.
		Discount discount = new BuyXItemGetYPercentDiscount(5,9,5);

		// Create new object of ShoppingCart default values
		ShoppingCart cart = new ShoppingCart();

		//set discount percentage depending on the number of items in the cart.
		cart.setDiscount(discount);

		//Get items from mockdatabase and add to the cart.
		Item item = imockDatabase.get(4);
		cart.addItem(item);

		//test the number of items in the shopping cart and retrieve its total value.
		assertEquals(1, cart.getItemCount());
		assertEquals(21.60, cart.getTotalCartValue(),0.01);
	}
	
	
    @Test
	public void testAddDifferentItemsAndAppyDiscountToTheCart(){

		//Created new object of Discount to implement minXitem, maxXitem and YDiscount.
		Discount discount = new BuyXItemGetYPercentDiscount(10,51,10);

		// Create new object of ShoppingCart default values
		ShoppingCart cart = new ShoppingCart();

		//Get items from mockdatabase and add to the cart.
		Item bread = imockDatabase.get(5);
		Item cereals = imockDatabase.get(6);
		cart.addItem(bread);
		cart.addItem(cereals);

		//set the discount percentage depending on the number of items in the cart.
		cart.setDiscount(discount);

		//test total number of items in the shopping cart and retrieve the total cart value.
		assertEquals(2, cart.getItemCount());
		assertEquals(37.34, cart.getTotalCartValue(),0.01);
	}
    
    @Test
	public void testAddLessItemsAndAppyNoDiscountToTheCart(){

		// Create new object of ShoppingCart default values
		ShoppingCart cart = new ShoppingCart();

		//Get items from mockdatabase and add to the cart.
		Item beans = imockDatabase.get(7);
		Item waffle = imockDatabase.get(8);
		cart.addItem(beans);
		cart.addItem(waffle);

		//set the discount percentage depending on the number of items in the cart.
		cart.setDiscount(new NoDiscount());

		//test total number of items in the shopping cart and retrieve the total cart value.
		assertEquals(2, cart.getItemCount());
		assertEquals(9.58, cart.getTotalCartValue(),0.01);
	}
    
    @Test
	public void testMembersDiscount(){

		// Create new object of ShoppingCart default values
    	ShoppingCart cart = new ShoppingCart();

		//Created new object of Discount to implement minXitem, maxXitem and YDiscount.
    	Discount discount = new BuyXItemGetYPercentDiscount(5,9,5);

		//Get items from mockdatabase and add to the cart
		Item milk = imockDatabase.get(9);
		Item eggs = imockDatabase.get(10);
		cart.addItem(eggs);
		cart.addItem(milk);

		//set the discount percentage depending on the number of items in the cart.
		cart.setDiscount(discount);

		// Added customer to the cart who is tax-exempt as being a member of the club.
		Customer customer =  new Customer(1, "Christine", "Kreger", true, true,new ShoppingCart());
		customer.setCart(cart);

		// test the total items scanned by this customer and retrive the total value.
		assertEquals(10.07, cart.getTotalCartValue(),0.01);

		// test is the customer is a member of the club.
		assertEquals(true, customer.isClubMember());

		//set additional 10% discount for member's of the shopping club.
		cart.setDiscount(new MembersClubDiscount(10));//Adding additional Members Discount

		// get the number of items scanned by the customer and calculate the final cart value.
		assertEquals(2, cart.getItemCount());
		assertEquals(20.16, cart.getTotalCartValue(),0.01);
	}
    
    @Test
    public void testTotalItemsInTheCart(){

		// Create new object of ShoppingCart default values
    	ShoppingCart cart = new ShoppingCart();
         populateCart(cart);

		//Customer who bought 50 or more items in a single shopping cart.
        Customer customer = new Customer(2,"Joe", "Napolitano", true, true,new ShoppingCart());
        customer.setCart(cart);

        // test if there are 50 items in the cart.
        // test - if there are more than 50 items then fail.
        // test if there are less than 50 items in the cart.
        assertEquals(false, customer.getCart().getItemCount() > 50);

		//Added 51 items of the single cart.
        cart.addItem(imockDatabase.get(51));

		// test if the total number of items in the single cart is more than 50 or not?
        assertEquals(true, customer.getCart().getItemCount()>50);

		//throw and exception if there are more than 50 items in a single shopping cart and display the message.
        exception.expect(IllegalItemException.class);
        exception.expectMessage("There are more than 50 items in the cart.");
         
        }

	private void populateCart(ShoppingCart cart) {
		for(int i=1;i<51;i++){
        	cart.addItem((Item)imockDatabase.get(i));
        }
	}
   
   @Test
   public void testApplyLocalSalesTax(){

	   // Create new object of ShoppingCart default values
	   	ShoppingCart cart = new ShoppingCart();
   		LocalSalesTax tax= new LocalSalesTax(4.5);
		Item item1 = imockDatabase.get(11);
		Item item2 = imockDatabase.get(12);
		cart.addItem(item1);
		cart.addItem(item2);
		Customer customer =  new Customer(3, "John", "Doe", true, false,new ShoppingCart());
		customer.setCart(cart);
		
		//Net purchase price before any discount.
		assertEquals(3.89, cart.getTotalCartValue(),0.01);

	   // Get the total items in the shopping cart.
		assertEquals(2, cart.getItemCount());

	   //test if there are more than 5 items and less than 9 items then No Discount is provided.
		assertEquals(false,(cart.getItemCount()>5 && cart.getItemCount()<9));
		cart.setDiscount(new NoDiscount());//No additional Discount

	   //test if the total items in the cart are more than 10 and less than 50 then provide 10% discount.
		assertEquals(false,(cart.getItemCount()>10 && cart.getItemCount() < 50));
		cart.setDiscount(new BuyXItemGetYPercentDiscount(10, 50, 10));

	   // test if the customer is the member of the club.
		assertEquals(true, customer.isClubMember());

	   // provide extra 10 percent discount for member's of the club.
		cart.setDiscount(new MembersClubDiscount(10));

	   // test the items in the cart and apply all the discounts as applicable and calculate the total cart value.
		assertEquals(7.78, cart.getTotalCartValue(),0.01);
		
		// test is the customer is tax exempt or not.
		assertEquals(false, customer.isTaxExempt());
		
		//Adding sales Tax to the cart total.
		cart.setSalesTax(tax);
		assertEquals(4.065, cart.getTotalCartValue(),0.01);
	   
   }


   
}


